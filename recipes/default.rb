#
# Cookbook Name:: apps-javaws
# Recipe:: default
#
# Copyright 2013, Brian Webb
#
# All rights reserved - Do Not Redistribute
#

include_recipe "network-javaws"
include_recipe "compute-base"
include_recipe "compute-java"
include_recipe "appserv-tomcat"
