name             'apps-javaws'
maintainer       'Brian Webb'
maintainer_email '@gmail.com'
license          'All rights reserved'
description      'Installs/Configures apps-javaws'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
depends          'network-javaws'
depends          'compute-base'
depends          'compute-java'
depends          'appserv-tomcat'
